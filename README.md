# Metro Kata (# 1)

An array of hotels containing a name, price per night & available days has been provided in `hotels.js`. The task is to filter the hotels based on the below criteria and output a new array containing _just_ the names of the hotels that match the search criteria.

Search criteria:

* The maximum price per night should be less than or equal to 100.00
* The hotel must have days available before 1st March 2016

## Rules

* Code should be JavaScript (feel free to use JSBin)
* Please spend no longer than 30 minutes attempting the kata - doesn't matter if you can't get to the end!


## Solution

Install Jasmine by running 'npm install -g jasmine' in terminal

In the root directory, run 'jasmine' to execute the specs
