var Search = function(hotels) 
{
    this.hotels = Array.isArray(hotels) ? hotels : [];
}

Search.prototype.filterByMaxPrice = function(maxPrice) {
    if (isNaN(maxPrice))
        return this.hotels;

    function maxPriceComparison(hotel) {
        return hotel.pricePerNight <= maxPrice;
    }

    return this.hotels.filter(maxPriceComparison)
}

Search.prototype.filterByMaxDate = function(maxDate) {
    if (!(maxDate instanceof Date))
        return this.hotels;

    function dateComparison(dateVal) {
        return dateVal.date < maxDate;
    }

    function maxDateComparison(hotel) {
        return hotel.availability.find(dateComparison) !== undefined;
    }

    return this.hotels.filter(maxDateComparison)
}

module.exports = Search;
