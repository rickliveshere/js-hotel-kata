var hotels = [
    {
        name: "Hilton Manchester",
        pricePerNight: 120.00,
        availability: [
            { date: new Date(2016, 1, 20) },
            { date: new Date(2016, 1, 21) },
            { date: new Date(2016, 1, 22)}
        ]
    },

    {
        name: "The Place Hotel",
        pricePerNight: 85.00,
        availability: [
            { date: new Date(2016, 1, 25) },
            { date: new Date(2016, 1, 27) },
            { date: new Date(2016, 1, 28) }
        ]
    },

    {
        name: "Radisson Blu",
        pricePerNight: 150.00,
        availability: [
            { date: new Date(2016, 2, 1) },
            { date: new Date(2016, 2, 5) },
            { date: new Date(2016, 2, 6) }
        ]
    },

    {
        name: "Holiday Inn",
        pricePerNight: 65.00,
        availability: [
            { date: new Date(2016, 1, 22) },
            { date: new Date(2016, 1, 23) },
            { date: new Date(2016, 1, 24) }
        ]
    },

    {
        name: "Travelodge Manchester",
        pricePerNight: 40.00,
        availability: [
            { date: new Date(2016, 2, 2) },
            { date: new Date(2016, 2, 3) },
            { date: new Date(2016, 2, 5) }
        ]
    }
];

module.exports = hotels;
