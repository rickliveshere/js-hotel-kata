var Search = require('../src/search');
var hotels = require('../src/hotels');

describe("Hotels returned from a search", function() {
  var search = new Search(hotels);

  it("must have a maximum price per night less than or equal to 100.00", function() {
    
  	var maxPrice = 100;
    var filteredHotels = search.filterByMaxPrice(maxPrice);

    for (var x = 0; x < filteredHotels.length; x++)
    {
    	expect(filteredHotels[x].pricePerNight <= maxPrice).toBe(true);
    }
  });

  it("must have days available before 1st March 2016", function() {
    var maxDate = new Date(2016, 2, 1);

    var filteredHotels = search.filterByMaxDate(maxDate);
    
   	for (var x = 0; x < filteredHotels.length; x++)
    {
    	for (var y = 0; y < filteredHotels[x].availability.length; y++)
    	{
    		expect(filteredHotels[x].availability[y].date < maxDate).toBe(true);
    	}
    }
  });
});